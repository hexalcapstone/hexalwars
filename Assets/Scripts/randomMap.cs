﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using UnityEngine;

//[assembly: AssemblyVersion("4.3.2.1")]


/*
    Jason's guide to this shit coding:
    This makes a map
    Spawns bases bases on the map dpending on the amount of players
    can make buildings
    can make units
    make building spots
*/

public class RandomMap : MonoBehaviour
{

    public GameObject tile;
    public int radius; //center is [radius - 1, radius -1] Arrays start at [0,0]
    public int baseHeight; //How big the bases gonna be, think square then shift one side
    public int baseWidth;
    public float objSize; //how big the tiles are
    public GameObject[,] map; //look up flat top Axial coordinates
    private int up; //for my shit camera
    private int ri;
    public int numPlayers; //number of players
    private Color prevColor; //for the camrea movement
    public float mountains; // 0f - 1f chance that a mountain will appear
    public Boolean bigMountains; // pits 3+ mountain tiles next to each other
    public GameObject sphere; // for testing
    public List<GameObject> units; //Array all the units
    public List<GameObject> buildings; //Array all the building
    public List<GameObject> ghostBuildings; //will delete later
    public GameObject testB;
    public GameObject baseBuilding;
    public GameObject spawnBuilding;
    public GameObject mnt; // Mountain object

	public GameObject mine;
	public AudioSource sounds;
	public AudioClip built;

    /* Use this for initialization
     * Does Prechecks of inputs
     * Starts the game up
     */
    void Start()
    {
        GameObject menu = GameObject.Find("Menu options");
        radius = menu.GetComponent<GameInfo>().MapRadius;
        numPlayers = menu.GetComponent<GameInfo>().Players;
        baseWidth = menu.GetComponent<GameInfo>().width;
        baseHeight = menu.GetComponent<GameInfo>().height;
        //prechecks
        if (radius < 7) radius = 7;
        if (baseHeight < 2) baseHeight = 2;
        if (baseWidth < 2) baseWidth = 2;
        if (numPlayers > 6)
        {
            numPlayers = 6;
        }
        else if (numPlayers < 0)
        {
            numPlayers = 0;
        }
        if (baseHeight < 1)
        {
            baseHeight = 1;
        }
        else if (baseHeight > radius / 2 - 1)
        {
            baseHeight = radius / 2 - 1;
        }
        if (baseWidth < 1)
        {
            baseWidth = 1;
        }
        else if (baseWidth > radius / 2 - 1)
        {
            baseWidth = radius / 2 - 1;
        }

        Instantiator();

        map = new GameObject[radius * 2, radius * 2];
        TileAssign();

        BaseAssigning();

        MountainAssign();

        /*up = radius - 1;
        ri = radius - 1;
        prevColor = map[radius - 1, radius - 1].GetComponent<Renderer>().material.color;
        map[radius - 1, radius - 1].GetComponent<Renderer>().material.color = Color.black;
        transform.position = map[ri, up].transform.position + transform.forward * -100;*/

        units = new List<GameObject>();
        buildings = new List<GameObject>();
        ghostBuildings = new List<GameObject>();

        BaseBuildings();

        //CreateSpheres();
        //objInstant();

		GameObject.Find ("PlayerController").GetComponent<PlayerController> ().initPC ();
    }


    /*Intial set up to deploy bases that can be attacked, and spawn points
     * bases stay in the building list
     * spawns get removed from building list
     * bases spawn on the corners
     * spawns spawn in the tile closest to the center
     */
    private void BaseBuildings()
    {
        GameObject[,] pTiles = PlayerTiles();
        //Base Fort
        for (int q = 0; q < numPlayers; q++)
        {
            for (int w = 0; w < pTiles.GetLength(1); w++)
            {
                if (pTiles[q, w].GetComponent<Tile>().GetX() == 0 && pTiles[q, w].GetComponent<Tile>().GetY() == 0)
                {
                    Build(baseBuilding, 0, 0);
                }
                if (pTiles[q, w].GetComponent<Tile>().GetX() == radius - 1 && pTiles[q, w].GetComponent<Tile>().GetY() == 0)
                {
                    Build(baseBuilding, radius - 1, 0);
                }
                if (pTiles[q, w].GetComponent<Tile>().GetX() == 0 && pTiles[q, w].GetComponent<Tile>().GetY() == radius - 1)
                {
                    Build(baseBuilding, 0, radius - 1);
                }
                if (pTiles[q, w].GetComponent<Tile>().GetX() == radius * 2 - 2 && pTiles[q, w].GetComponent<Tile>().GetY() == radius - 1)
                {
                    Build(baseBuilding, radius * 2 - 2, radius - 1);
                }
                if (pTiles[q, w].GetComponent<Tile>().GetX() == radius - 1 && pTiles[q, w].GetComponent<Tile>().GetY() == radius * 2 - 2)
                {
                    Build(baseBuilding, radius - 1, radius * 2 - 2);
                }
                if (pTiles[q, w].GetComponent<Tile>().GetX() == radius * 2 - 2 && pTiles[q, w].GetComponent<Tile>().GetY() == radius * 2 - 2)
                {
                    Build(baseBuilding, radius * 2 - 2, radius * 2 - 2);
                }
            }
        }

        //Unit Spawner
        /*for (int q = 0; q < numPlayers; q++)
        {
            for (int w = 0; w < pTiles.GetLength(1); w++)
            {
                int bw = baseWidth - 1; //for this purpose only
                int bh = baseHeight - 1;
                if (pTiles[q, w].GetComponent<Tile>().GetX() == 0 && pTiles[q, w].GetComponent<Tile>().GetY() == 0)
                {
                    buildings.Remove(Build(spawnBuilding, bw, bh));
                }
                if (pTiles[q, w].GetComponent<Tile>().GetX() == radius - 1 && pTiles[q, w].GetComponent<Tile>().GetY() == 0)
                {
                    buildings.Remove(Build(spawnBuilding, radius - 1, bh));
                }
                if (pTiles[q, w].GetComponent<Tile>().GetX() == 0 && pTiles[q, w].GetComponent<Tile>().GetY() == radius - 1)
                {
                    buildings.Remove(Build(spawnBuilding, bw, radius - 1));
                }
                if (pTiles[q, w].GetComponent<Tile>().GetX() == radius * 2 - 2 && pTiles[q, w].GetComponent<Tile>().GetY() == radius - 1)
                {
                    buildings.Remove(Build(spawnBuilding, radius * 2 - 2 - bw, radius - 1));
                }
                if (pTiles[q, w].GetComponent<Tile>().GetX() == radius - 1 && pTiles[q, w].GetComponent<Tile>().GetY() == radius * 2 - 2)
                {
                    buildings.Remove(Build(spawnBuilding, radius - 1, radius * 2 - 2 - bh));
                }
                if (pTiles[q, w].GetComponent<Tile>().GetX() == radius * 2 - 2 && pTiles[q, w].GetComponent<Tile>().GetY() == radius * 2 - 2)
                {
                    buildings.Remove(Build(spawnBuilding, radius * 2 - 2 - bw, radius * 2 - 2- bh));
                }
            }
        }*/
    }

    /*A method that takes all the tiles assigned for all the players and organises them, returning GAmeObject[player, players tiles]
     * tNums will end up as width * height
     */
    public GameObject[,] PlayerTiles()
    {
        RandomMap rMap = GameObject.Find("MapGen").GetComponent<RandomMap>();
        int temp = rMap.baseHeight * rMap.baseWidth;
        GameObject[,] pTiles = new GameObject[rMap.numPlayers, temp];
        //Debug.Log("Player: " + rMap.numPlayers + " NumPlayers: " + pTiles.GetLength(0) + " Tiles: " + pTiles.GetLength(1));
        int tNum0 = 0;
        int tNum1 = 0;
        int tNum2 = 0;
        int tNum3 = 0;
        int tNum4 = 0;
        int tNum5 = 0;

        for (int x = 0; x < rMap.radius * 2; x++)
        {
            for (int y = 0; y < rMap.radius * 2; y++)
            {
                if (rMap.map[x, y] != null && rMap.map[x, y].GetComponent<Tile>().GetTeam() != -1)
                {
                    // green = 0, blue = 1, red = 2, magenta = 3, yellow = 4, cyan = 5     ARRAYS START AT 0
                    int team = rMap.map[x, y].GetComponent<Tile>().GetTeam();
                    if (team == 0)
                    {
                        ////Debug.Log("team: " + team + " tNum0: " + tNum0 + " X: " + x + " Y: " + y);
                        pTiles[team, tNum0++] = rMap.map[x, y];
                    }
                    else if (team == 1)
                        pTiles[team, tNum1++] = rMap.map[x, y];
                    else if (team == 2)
                        pTiles[team, tNum2++] = rMap.map[x, y];
                    else if (team == 3)
                        pTiles[team, tNum3++] = rMap.map[x, y];
                    else if (team == 4)
                        pTiles[team, tNum4++] = rMap.map[x, y];
                    else if (team == 5)
                        pTiles[team, tNum5++] = rMap.map[x, y];
                }
            }
        }

        return pTiles;
    }


    //creates buildable buildings possible
    public void GhostBuild(GameObject ghost)
    {
        int player = GameObject.Find("GameControls").GetComponent<TurnController>().GetPlayerID();
        GameObject[,] buildTiles = PlayerTiles();
		bool once = true;
        for (int p = 0; p < buildTiles.GetLength(1); p++)
        {
            GameObject tile = buildTiles[player, p];


            if (tile.GetComponent<Tile>().GetBuilding() == null && NoUnitPresent(tile.GetComponent<Tile>().GetX(), tile.GetComponent<Tile>().GetY()))
            {
                GameObject b = Instantiate(ghost, tile.transform.position + Vector3.up, Quaternion.identity);
                ghostBuildings.Add(b);
				if (ghost.name == mine.name  && once) {
					TurnController tc = GameObject.Find ("GameControls").GetComponent<TurnController> ();
					tc.pCon.incInc (tc.GetPlayerID());
					once = false;
				}
            }
        }
    }

    //making sure none the buildable tiles have any units
    public bool NoUnitPresent(int v1, int v2)
    {
        foreach(GameObject unit in units)
        {
            int x = unit.GetComponent<UnitBehave>().GetPos()[0];
            int y = unit.GetComponent<UnitBehave>().GetPos()[1];

            if (x == v1 && y == v2)
            {
                return false;
            }
        }
        return true;
    }

    //destroys that buildable buildings
    public void DestroyGhosts()
    {
        foreach (GameObject gBuild in ghostBuildings)
        {
            DestroyImmediate(gBuild);
        }
        ghostBuildings.Clear();
    }

    //builds the build passed into it and places it in the buildings list
    public GameObject Build(GameObject building, int x, int y)
    {
			
		sounds.PlayOneShot (built);

        GameObject buildin = Instantiate(building, map[x, y].transform.position + Vector3.up, Quaternion.identity);
        buildin.GetComponentInChildren<Renderer>().material.color = map[x, y].GetComponent<Renderer>().material.color;
        buildin.GetComponent<buildingStats>().SetTeam(map[x, y].GetComponent<Tile>().GetTeam());
        buildin.GetComponent<buildingStats>().SetX(x);
        buildin.GetComponent<buildingStats>().SetY(y);
        buildings.Add(buildin);
        map[x, y].GetComponent<Tile>().SetBuilding(buildin);
        return buildin;
    }


    //use this to instantate a unit. Move is how many tiles it can move before the next unit.
    public void ObjInstant(GameObject unitType, int move, int HP, int posX, int posY, int tarX, int tarY)
    {
        GameObject unit = Instantiate(unitType, map[posX, posY].transform.position + Vector3.up, Quaternion.identity);

        
        //unit.GetComponent<Renderer>().material.color = map[posX, posY].GetComponentInChildren<Renderer>().material.color;

        //unit.GetComponent<UnitBehave>().Start();
        unit.GetComponent<UnitBehave>().SetMP(1);
        unit.GetComponent<UnitBehave>().SetHP(HP);
        int[] pos = new int[2];
        pos[0] = posX;
        pos[1] = posY;
        unit.GetComponent<UnitBehave>().SetPos(pos);
        unit.GetComponent<UnitBehave>().SetTeam(map[posX, posY].GetComponent<Tile>().GetTeam());
        //unit.GetComponent<UnitBehave>().SetTarget(tarX, tarY);
        unit.GetComponent<UnitBehave>().RandomTarget(); //uncomment line above if you want to set target
        units.Add(unit);
    }

    //this was just to create test dummies for movement
    private void CreateSpheres()
    {
        ObjInstant(sphere, 1, 2, 0, 0, radius - 1, radius * 2 - 2);
        ObjInstant(sphere, 1, 2, 0, radius - 1, radius * 2 - 2, radius * 2 - 2);
        ObjInstant(sphere, 1, 2, radius - 1, radius * 2 - 2, radius * 2 - 2, radius - 1);
        ObjInstant(sphere, 1, 2, radius * 2 - 2, radius * 2 - 2, radius - 1, 0);
        ObjInstant(sphere, 1, 2, radius * 2 - 2, radius - 1, 0, 0);
        ObjInstant(sphere, 1, 2, radius - 1, 0, 0, radius - 1);
    }

    //Assigns mountains to tiles
    private void MountainAssign()
    {
        //creates the border of mountains around the map
        for (int w = 0; w < radius; w++)
        {
            if (map[w, 0].GetComponent<Renderer>().material.color == Color.white)
            {
                map[w, 0].GetComponent<Renderer>().material.color = Color.gray;
                //Instantiate(mnt, map[w, 0].GetComponent<Transform>().position, Quaternion.identity);//
            }
            if (map[0, w].GetComponent<Renderer>().material.color == Color.white)
            {
                map[0, w].GetComponent<Renderer>().material.color = Color.gray;
                //Instantiate(mnt, map[0, w].GetComponent<Transform>().position, Quaternion.identity);//
            }
            if (map[radius - 1 + w, w].GetComponent<Renderer>().material.color == Color.white)
            {
                map[radius - 1 + w, w].GetComponent<Renderer>().material.color = Color.gray;
                //Instantiate(mnt, map[radius - 1 + w, w].GetComponent<Transform>().position, Quaternion.identity);//
            }
            if (map[w, radius - 1 + w].GetComponent<Renderer>().material.color == Color.white)
            {
                map[w, radius - 1 + w].GetComponent<Renderer>().material.color = Color.gray;
                //Instantiate(mnt, map[w, radius - 1 + w].GetComponent<Transform>().position, Quaternion.identity);
            }
            if (map[radius * 2 - 2, radius - 1 + w].GetComponent<Renderer>().material.color == Color.white)
            {
                map[radius * 2 - 2, radius - 1 + w].GetComponent<Renderer>().material.color = Color.gray;
               //Instantiate(mnt, map[radius * 2 - 2, radius - 1 + w].GetComponent<Transform>().position, Quaternion.identity);
            }
            if (map[radius - 1 + w, radius * 2 - 2].GetComponent<Renderer>().material.color == Color.white)
            {
                map[radius - 1 + w, radius * 2 - 2].GetComponent<Renderer>().material.color = Color.gray;
                //Instantiate(mnt, map[radius - 1 + w, radius * 2 - 2].GetComponent<Transform>().position, Quaternion.identity);
            }
        }
        //if yoy want a mountains with more than 3 neighbour mountains
        if (bigMountains)
        {
            for (int m = 0; m < radius - (baseHeight + baseWidth) / 3; m++)
            {
                int x = 0;
                int y = radius * 2 - 2;
                while (map[x, y] == null)
                {
                    x = UnityEngine.Random.Range((baseWidth + 2), (radius * 2 - 4 - baseWidth));
                    y = UnityEngine.Random.Range((baseHeight + 2), (radius * 2 - 4 - baseHeight));
                }
                for (int p = -1; p < 2; p++)
                {
                    for (int q = -1; q < 2; q++)
                    {
                        if (map[x + p, y + q] != null && map[x + p, y + q].GetComponent<Renderer>().material.color == Color.white && UnityEngine.Random.Range(0f, 1f) < mountains)
                            map[x + p, y + q].GetComponent<Renderer>().material.color = Color.grey;
                            //Instantiate(mnt, map[x + p, y + q].GetComponent<Transform>().position, Quaternion.identity);
                    }
                }
            }
        }
        //Actual mountain ranges are made
        for (int x = 0; x < radius * 2 - 2; x++)
        {
            for (int y = 0; y < radius * 2 - 2; y++)
            {
                if (map[x, y] != null && map[x, y].GetComponent<Renderer>().material.color == Color.white)
                {
                    int counter = 0;
                    if (x - 1 > 0 && UnityEngine.Random.Range(0.0f, 1.0f) < mountains)
                    {
                        if (map[x - 1, y] == null || map[x - 1, y].GetComponent<Renderer>().material.color == Color.white)
                        {
                            counter += 2;
                        }
                        else if (map[x - 1, y] != null && map[x - 1, y].GetComponent<Renderer>().material.color != Color.grey)
                        {
                            counter += 1;
                        }
                    }
                    if (x - 1 > 0 && y - 1 > 0 && UnityEngine.Random.Range(0.0f, 1.0f) < mountains)
                    {
                        if (map[x - 1, y - 1] == null || map[x - 1, y - 1].GetComponent<Renderer>().material.color == Color.white)
                        {
                            counter += 2;
                        }
                        else if (map[x - 1, y - 1] != null && map[x - 1, y - 1].GetComponent<Renderer>().material.color != Color.grey)
                        {
                            counter += 1;
                        }
                    }
                    if (y - 1 > 0 && UnityEngine.Random.Range(0.0f, 1.0f) < mountains)
                    {
                        if (map[x, y - 1] == null || map[x, y - 1].GetComponent<Renderer>().material.color == Color.white)
                        {
                            counter += 2;
                        }
                        else if (map[x, y - 1] != null && map[x, y - 1].GetComponent<Renderer>().material.color != Color.grey)
                        {
                            counter += 1;
                        }
                    }
                    if (counter > 2)
                    {
                        map[x, y].GetComponent<Renderer>().material.color = Color.grey;
                        //Instantiate(mnt, map[x, y].GetComponent<Transform>().position, Quaternion.identity);
                    }
                }
            }
        }

        //balancing, makes path from corner to corner open
        for (int e = 1; e < radius * 2 - 2; e++)
        {
            if (map[radius - 1, e].GetComponent<Renderer>().material.color == Color.grey)
            {
                map[radius - 1, e].GetComponent<Renderer>().material.color = Color.white;
            }
            if (map[e, radius - 1].GetComponent<Renderer>().material.color == Color.grey)
            {
                map[e, radius - 1].GetComponent<Renderer>().material.color = Color.white;
            }
            if (map[e, e].GetComponent<Renderer>().material.color == Color.grey)
            {
                map[e, e].GetComponent<Renderer>().material.color = Color.white;
            }
        }
        var objects = GameObject.FindGameObjectsWithTag("Tiles");
        var objectCount = objects.Length;
        foreach (var obj in objects)
        {
            if(obj.GetComponent<Renderer>().material.color == Color.grey)
            {
                Instantiate(mnt, obj.GetComponent<Transform>().position, Quaternion.identity);
            }
        }
    }

    //gives corners to players, making them as random as possible. But for 3 and less players they will appear on opposite ends.
    private void BaseAssigning()
    {
        //deciding corners
        int[] corner = new int[6];
        for(int i = 0; i < 6; i++)
        {
            corner[i] = -1;
        }
        if (numPlayers > 0)
        {
            corner[0] = UnityEngine.Random.Range(0, 6);
            if (numPlayers == 2)
            {
                if (corner[0] < 3)
                {
                    corner[1] = corner[0] + 3;
                }
                else
                {
                    corner[1] = corner[0] - 3;
                }
            }
            else if (numPlayers == 3)
            {
                if (UnityEngine.Random.Range(0, 2) == 0) // just wanted to make the selection place random
                {
                    if (corner[0] > 3)
                    {
                        corner[1] = corner[0] - 4;
                        corner[2] = corner[0] - 2;
                    }
                    else
                    {
                        corner[1] = corner[0] + 2;
                        if (corner[0] < 2)
                        {
                            corner[2] = corner[0] + 4;
                        }
                        else
                        {
                            corner[2] = corner[0] - 2;
                        }
                    }
                }
                else
                {
                    if (corner[0] < 2)
                    {
                        corner[1] = corner[0] + 4;
                        corner[2] = corner[0] + 2;
                    }
                    else
                    {
                        corner[1] = corner[0] - 2;
                        if (corner[0] > 3)
                        {
                            corner[2] = corner[0] - 4;
                        }
                        else
                        {
                            corner[2] = corner[0] + 2;
                        }
                    }
                }
            }
            /*else if(numPlayers == 4) I'll sort this out later to make 4 players more fair
            {
                int[] temp = new int[3];
                for(int i = 0; i < 3; i++)
                {
                    temp[i]
                }
                temp = RandomArr(temp);
                int p1 = corner[0];
                float rightS = UnityEngine.Random.Range(0f, 1f);
                for (int i = 0; i < 3; i++)
                {
                    if(rightS >= 0.5)
                    {
                        corner[temp[i]] =
                    }
                    else
                    {
                        corner[temp[i]] =
                    }
                }
            }*/
            else //all random now
            {
                for (int t = 0; t < 6; t++)
                {
                    corner[t] = t;
                }
                RandomArr(corner);
            }
        }

        //making the bases
        for (int c = 0; c < 6; c++)
        {
            if (c < numPlayers)
            {
                if (corner[c] == 0)
                {
                    for (int h = 0; h < baseHeight; h++)
                    {
                        for (int w = 0; w < baseWidth; w++)
                        {
                            //Debug.Log(w + ", " + h);
                            PlayerColor(map[w, h], c);
                        }
                    }
                }
                if (corner[c] == 1)
                {
                    int r = 0;
                    for (int h = 0; h < baseHeight; h++)
                    {
                        for (int w = 0; w < baseWidth; w++)
                        {
                            //Debug.Log((radius - 1 - w + r) + ", " + h);
                            PlayerColor(map[radius - 1 - w + r, h], c);
                        }
                        r++;
                    }
                }
                if (corner[c] == 2)
                {
                    int r = 0;
                    for (int w = 0; w < baseWidth; w++)
                    {
                        for (int h = 0; h < baseHeight; h++)
                        {
                            //Debug.Log((radius * 2 - 2 - w) + ", " + (radius -1 + h - r));
                            PlayerColor(map[radius * 2 - 2 - w, radius - 1 + h - r], c);
                        }
                        r++;
                    }
                }
                if (corner[c] == 3)
                {
                    for (int h = 0; h < baseHeight; h++)
                    {
                        for (int w = 0; w < baseWidth; w++)
                        {
                            //Debug.Log((radius * 2 - 2 - w) + ", " + (radius * 2 - 2 - h));
                            PlayerColor(map[radius * 2 - 2 - w, radius * 2 - 2 - h], c);
                        }
                    }
                }
                if (corner[c] == 4)
                {
                    int r = 0;
                    for (int h = 0; h < baseHeight; h++)
                    {
                        for (int w = 0; w < baseWidth; w++)
                        {
                            PlayerColor(map[radius - 1 + w - r, radius * 2 - 2 - h], c);
                        }
                        r++;
                    }
                }
                if (corner[c] == 5)
                {
                    //Debug.Log(radius - 1);
                    PlayerColor(map[0, radius - 1], c);
                    int r = 0;
                    for (int w = 0; w < baseWidth; w++)
                    {
                        for (int h = 0; h < baseHeight; h++)
                        {
                            PlayerColor(map[w, radius - 1 - h + r], c);
                        }
                        r++;
                    }
                }
            }
        }

    }

    //assigns the colors to the object
    private void PlayerColor(GameObject game, int c)
    {
        if (true)
        {
            game.GetComponent<Tile>().SetTeam(c);
            if (c == 0)
            {
                game.GetComponent<Renderer>().material.color = Color.green;
            }
            if (c == 1)
            {
                game.GetComponent<Renderer>().material.color = Color.blue;
            }
            if (c == 2)
            {
                game.GetComponent<Renderer>().material.color = Color.red;
            }
            if (c == 3)
            {
                game.GetComponent<Renderer>().material.color = Color.magenta;
            }
            if (c == 4)
            {
                game.GetComponent<Renderer>().material.color = Color.yellow;
            }
            if (c == 5)
            {
                game.GetComponent<Renderer>().material.color = Color.cyan;
            }
        }
    }

    //for the corner assigning, creates a random array for assigning random corners
    private int[] RandomArr(int[] arr)
    {
        for (int p = 0; p < 2; p++)
        {
            for (int t = 0; t < arr.Length; t++)
            {
                int temp = arr[t];
                int ran = UnityEngine.Random.Range(0, arr.Length);
                arr[t] = arr[ran];
                arr[ran] = temp;
            }
        }
        return arr;
    }

    //assigns stuff to the tiles
    private void TileAssign()
    {
        //float y = 0.1f;
        foreach (GameObject t in GameObject.FindGameObjectsWithTag("Tiles"))
        {
            float x = (t.transform.position.x / objSize / 5 * 6);
            float z = (t.transform.position.z / objSize) + x * 0.5f;
            if (x % 1 >= 0.5)
            {
                x = x - x % 1 + 1;
            }
            else
            {
                x = x - x % 1;
            }
            if (z % 1 >= 0.5)
            {
                z = z - z % 1 + 1;
            }
            else
            {
                z = z - z % 1;
            }
            t.GetComponent<Tile>().SetC((int)x, (int)z);
            t.GetComponent<Tile>().SetTeam(-1);
            map[(int)x, (int)z] = t;
            t.GetComponent<Renderer>().material.color = Color.white;
            //y+=1;
        }
    }

    //The start of map
    private void Instantiator()
    {
        

        //fill in that last gap
        for (int x = 0; x < radius * 2 - 1; x++)
        {
            if (x % 2 == 1)
            {
                GameObject t = Instantiate(tile, new Vector3(x * objSize * 5 / 6, 0, 0 * objSize - objSize / 2), Quaternion.identity);
                t.GetComponent<Renderer>().material.color = Color.white;
                t.transform.parent = GameObject.Find("Map").transform;
            }
        }
        //straight up
        for (int y = 0; y < radius; y++)
        {
            for (int x = 0; x < radius * 2 - 1; x++)
            {
                if (x % 2 == 0)
                {
                    GameObject t = Instantiate(tile, new Vector3(x * objSize * 5 / 6, 0, y * objSize), Quaternion.identity);
                    t.transform.parent = GameObject.Find("Map").transform;
                }
                else
                {
                    GameObject t = Instantiate(tile, new Vector3(x * objSize * 5 / 6, 0, y * objSize + objSize / 2), Quaternion.identity);
                    t.transform.parent = GameObject.Find("Map").transform;
                }
            }
        }
        //converge at top and bot
        int temp = 1;
        for (int y = radius; y < radius * 2 - 1; y++)
        {
            for (int x = 0; x < radius * 2 - 1; x++)
            {
                if (x % 2 == 0 && x > temp && x < radius * 2 - temp - 2)
                {
                    GameObject t = Instantiate(tile, new Vector3(x * objSize * 5 / 6, 0, y * objSize), Quaternion.identity);
                    GameObject t1 = Instantiate(tile, new Vector3(x * objSize * 5 / 6, 0, (-y + radius - 1) * objSize), Quaternion.identity);
                    t.transform.parent = GameObject.Find("Map").transform;
                    t1.transform.parent = GameObject.Find("Map").transform;
                }
                else if (x > temp && x < radius * 2 - temp - 2)
                {
                    GameObject t = Instantiate(tile, new Vector3(x * objSize * 5 / 6, 0, y * objSize + objSize / 2), Quaternion.identity);
                    GameObject t1 = Instantiate(tile, new Vector3(x * objSize * 5 / 6, 0, (-y + radius - 1) * objSize - objSize / 2), Quaternion.identity);
                    t.transform.parent = GameObject.Find("Map").transform;
                    t1.transform.parent = GameObject.Find("Map").transform;
                }
            }
            temp++;
            temp++;
        }
    }

    // Update is called once per frame
    void Update()
    {
        /*if (Input.GetKeyDown(KeyCode.W) && up + 1 < radius * 2 && map[ri, up + 1] != null && map[ri, up + 1].GetComponent<Renderer>().material.color != Color.grey)
        if (Input.GetKeyDown(KeyCode.W) && up + 1 < radius * 2 && map[ri, up + 1] != null)
        {

            map[ri, up].GetComponent<Renderer>().material.color = prevColor;
            up++;
            prevColor = map[ri, up].GetComponent<Renderer>().material.color;
            map[ri, up].GetComponent<Renderer>().material.color = Color.black;
            transform.position = map[ri, up].transform.position + transform.forward * -100;
        }
        //if (Input.GetKeyDown(KeyCode.S) && up - 1 >= 0 && map[ri, up - 1] != null && map[ri, up - 1].GetComponent<Renderer>().material.color != Color.grey)
        if (Input.GetKeyDown(KeyCode.S) && up - 1 >= 0 && map[ri, up - 1] != null)
        {
            map[ri, up].GetComponent<Renderer>().material.color = prevColor;
            up--;
            prevColor = map[ri, up].GetComponent<Renderer>().material.color;
            map[ri, up].GetComponent<Renderer>().material.color = Color.black;
            transform.position = map[ri, up].transform.position + transform.forward * -100;
        }
        //if (Input.GetKeyDown(KeyCode.A) && ri - 1 >= 0 && map[ri - 1, up] != null && map[ri - 1, up].GetComponent<Renderer>().material.color != Color.grey)
        if (Input.GetKeyDown(KeyCode.A) && ri - 1 >= 0 && map[ri - 1, up] != null)
        {
            map[ri, up].GetComponent<Renderer>().material.color = prevColor;
            ri--;
            prevColor = map[ri, up].GetComponent<Renderer>().material.color;
            map[ri, up].GetComponent<Renderer>().material.color = Color.black;
            transform.position = map[ri, up].transform.position + transform.forward * -100;
        }
        //if (Input.GetKeyDown(KeyCode.D) && ri + 1 < radius * 2 && map[ri + 1, up] != null && map[ri + 1, up].GetComponent<Renderer>().material.color != Color.grey)
        if (Input.GetKeyDown(KeyCode.D) && ri + 1 < radius * 2 && map[ri + 1, up] != null)
        {
            map[ri, up].GetComponent<Renderer>().material.color = prevColor;
            ri++;
            prevColor = map[ri, up].GetComponent<Renderer>().material.color;
            map[ri, up].GetComponent<Renderer>().material.color = Color.black;
            transform.position = map[ri, up].transform.position + transform.forward * -100;
        }*/
        //reset the game
        if (Input.GetKeyDown(KeyCode.R))
        {
            UnityEngine.SceneManagement.SceneManager.LoadScene(1);
        }


        //Put it somewhere, I dont know
        if (Input.GetMouseButtonDown(0))
        { // if left button pressed...
            Ray ray = GameObject.Find("Main Camera").GetComponent<Camera>().ScreenPointToRay(Input.mousePosition);
            RaycastHit hit;
            if (Physics.Raycast(ray, out hit))
            {
                int x = -1;
                int y = -1;
                if (hit.collider.GetComponent<Tile>() != null)
                {
                    //Debug.Log("hit: " + hit.collider.GetComponent<Tile>().GetX() + ", " + hit.collider.GetComponent<Tile>().GetY());
                    x = hit.collider.GetComponent<Tile>().GetX();
                    y = hit.collider.GetComponent<Tile>().GetY();
                }
                else if (hit.collider.GetComponent<UnitBehave>() != null)
                {
                    //Debug.Log("hit: " + hit.collider.GetComponent<UnitBehave>().GetPos()[0] + ", " + hit.collider.GetComponent<UnitBehave>().GetPos()[1]);
                    x = hit.collider.GetComponent<UnitBehave>().GetPos()[0];
                    y = hit.collider.GetComponent<UnitBehave>().GetPos()[1];
                }
                else if (hit.collider.GetComponent<buildingStats>() != null)
                {
                    //Debug.Log("hit: " + hit.collider.GetComponent<buildingStats>().GetX() + ", " + hit.collider.GetComponent<buildingStats>().GetY());
                    x = hit.collider.GetComponent<buildingStats>().GetX();
                    y = hit.collider.GetComponent<buildingStats>().GetY();
                }
                //Debug.Log(x + ", " + y);
                 if (ghostBuildings.Count > 0)
                {
                    foreach(GameObject ghost in ghostBuildings)
                    {
                        //Debug.Log(ghost.transform.position.x + ", " + map[x,y].transform.position.x + ", " + ghost.transform.position.z + ", " + map[x, y].transform.position.z);
                        if(ghost.transform.position.x == map[x, y].transform.position.x && ghost.transform.position.z == map[x, y].transform.position.z)
                        {
                            //Debug.Log("triggered");
                            Build(ghost, x, y);
                        }
                    }
                    DestroyGhosts();
                    GameObject.Find("GameControls").GetComponent<BuildButtons>().buttonPressed(0);
                }
            }
            else
            {
                //Debug.Log("miss");
                DestroyGhosts();
                GameObject.Find("GameControls").GetComponent<BuildButtons>().buttonPressed(0);
            }
        }
    }
}
