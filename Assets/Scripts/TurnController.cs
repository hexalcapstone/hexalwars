﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TurnController : MonoBehaviour {
	private int turnState;
	public int playerCount;
	public UnityEngine.UI.Text playerText;
	public UnityEngine.UI.Text endText;
	public UnityEngine.UI.Button endButton;
	public PlayerController pCon;
    public camControl cam;

	public AudioSource sounds;
	public AudioClip endTurnClip;
	public AudioClip endGame;

    private RandomMap rmap;
	float timer;
	bool waiter;





    //Get necessary Components
	void Start () {
        
        cam = GameObject.Find("Main Camera").GetComponent<camControl>();
		turnState = -1;
		playerCount = 6;
        rmap = GameObject.Find("MapGen").GetComponent<RandomMap>();
		timer = 0;
		waiter = false;
	}
	
	// Update is called once per frame
	void Update () {
        /*	if (waiter) {
                timer += Time.deltaTime;
            }*/
        //
        if (turnState < rmap.GetComponent<RandomMap>().numPlayers && turnState >= GameObject.Find("Menu options").GetComponent<GameInfo>().Players - GameObject.Find("Menu options").GetComponent<GameInfo>().AI)
        {
            AIChoice(turnState);
        }
        //Debug.Log(turnState);




        if (GameObject.Find("MapGen").GetComponent<RandomMap>().units.Count > 0 && pCon.living(turnState))
        {
            bool allFinished = true;
            foreach(GameObject unit in GameObject.Find("MapGen").GetComponent<RandomMap>().units)
            {
                if (!unit.GetComponent<UnitBehave>().finished)
                {
                    allFinished = false;
                    break;
                }
            }
            if (allFinished)
            {
                foreach (GameObject unit in GameObject.Find("MapGen").GetComponent<RandomMap>().units)
                {
                    unit.GetComponent<UnitBehave>().StartTurn();
                    unit.GetComponent<UnitBehave>().Move();

                }

                for (int p = rmap.units.Count - 2; p > -2; p--)
                {
                    GameObject unit = rmap.units[p + 1];
                    if (unit.GetComponent<UnitBehave>().GetHP() <= 0)
                    {
                        rmap.GetComponent<RandomMap>().units.Remove(unit);
                        DestroyImmediate(unit);
                        //units--;
                    }
                }

                //checking for each buildings death

                for (int p = rmap.buildings.Count - 2; p > -2; p--)
                {
                    GameObject building = rmap.buildings[p + 1];
                    if (building.GetComponent<buildingStats>().GetHP() <= 0)
                    {
                        rmap.GetComponent<RandomMap>().buildings.Remove(building);
                        DestroyImmediate(building);
                    }
                }
                /*waiter = true;
				while(waiter){
					if (timer >= 0.5f) {
						waiter = false;
						timer = 0;
					}
				}*/
                //yield return new WaitForSeconds(1);

                //delete once we have attack, unless you like it like this, then just delete the while loop part of this

                //Debug.Log("Outside");
                //turnState = -1;
                playerCount = 0;
                foreach (GameObject building in rmap.buildings)
                {
                    if (building.transform.name == "BASE(Clone)") playerCount++;
                    else
                    {
                        break;
                    }
                }
                //Debug.Log("Once again");

                if (playerCount <= 1)
                {
                    //Debug.Log("Why??");
                    int winner = -1;
                    foreach (GameObject building in rmap.buildings)
                    {
                        if (building.transform.name == "BASE(Clone)") winner = building.GetComponent<buildingStats>().GetTeam();
                        else
                        {
                            break;
                        }
                    }
					sounds.PlayOneShot (endGame);
                    GameObject.Find("Menu options").GetComponent<GameInfo>().winner = winner;
                    UnityEngine.SceneManagement.SceneManager.LoadScene(2);
                }
            }

        }
        else if(!endButton.interactable)
        {
            endButton.interactable = true;
            turnState = -1;
        }
	}




	/*public void pause(){
		StartCoroutine (EndTurn());
	}*/


    
    /*Jason Werners Player AI of real last minute programming*/
    private void AIChoice(int turnState)
    {
        if(((pCon.GetGold(turnState) < 15  && UnityEngine.Random.Range(0f, 1f) < 0.7)|| UnityEngine.Random.Range(0f, 1f) < 0.5) && pCon.GetGold(turnState) > 7)
        {//barrack and archery
            if (((pCon.GetGold(turnState) < 12 && UnityEngine.Random.Range(0f, 1f) < 0.7) || UnityEngine.Random.Range(0f, 1f) < 0.5) && pCon.GetGold(turnState) > 7)
            {//barrack
                int player = GetPlayerID();
                GameObject[,] buildTiles = rmap.PlayerTiles();
                for (int p = 0; p < buildTiles.GetLength(1); p++)
                {
                    GameObject tile = buildTiles[player, p];
                    if (tile.GetComponent<Tile>().GetBuilding() == null && rmap.NoUnitPresent(tile.GetComponent<Tile>().GetX(), tile.GetComponent<Tile>().GetY()))
                    {
                        rmap.Build(GameObject.Find("GameControls").GetComponent<BuildButtons>().barrack, tile.GetComponent<Tile>().GetX(), tile.GetComponent<Tile>().GetY());
                        pCon.goldChange(GetPlayerID(), -8);
                        break;
                    }
                }

            }
            else if (UnityEngine.Random.Range(0f, 1f) < 0.7 && pCon.GetGold(turnState) > 11)
            {//archery
                int player = GetPlayerID();
                GameObject[,] buildTiles = rmap.PlayerTiles();
                for (int p = 0; p < buildTiles.GetLength(1); p++)
                {
                    GameObject tile = buildTiles[player, p];
                    if (tile.GetComponent<Tile>().GetBuilding() == null && rmap.NoUnitPresent(tile.GetComponent<Tile>().GetX(), tile.GetComponent<Tile>().GetY()))
                    {
                        rmap.Build(GameObject.Find("GameControls").GetComponent<BuildButtons>().range, tile.GetComponent<Tile>().GetX(), tile.GetComponent<Tile>().GetY());
                        pCon.goldChange(GetPlayerID(), -12);
                        break;
                    }
                }
            }
        }
        else if ( UnityEngine.Random.Range(0f, 1f) < 0.7 && pCon.GetGold(turnState) > 14)
        {//special and mine
            if (((pCon.GetGold(turnState) < 20 && UnityEngine.Random.Range(0f, 1f) < 0.7) || UnityEngine.Random.Range(0f, 1f) < 0.5) && pCon.GetGold(turnState) > 14)
            {//mine
                int player = GetPlayerID();
                GameObject[,] buildTiles = rmap.PlayerTiles();
                for (int p = 0; p < buildTiles.GetLength(1); p++)
                {
                    GameObject tile = buildTiles[player, p];
                    if (tile.GetComponent<Tile>().GetBuilding() == null && rmap.NoUnitPresent(tile.GetComponent<Tile>().GetX(), tile.GetComponent<Tile>().GetY()))
                    {
                        rmap.Build(GameObject.Find("GameControls").GetComponent<BuildButtons>().mine, tile.GetComponent<Tile>().GetX(), tile.GetComponent<Tile>().GetY());
                        pCon.goldChange(GetPlayerID(), -15);
                        break;
                    }
                }
            }
            else if (UnityEngine.Random.Range(0f, 1f) < 0.7 && pCon.GetGold(turnState) > 19)
            {//special
                int player = GetPlayerID();
                GameObject[,] buildTiles = rmap.PlayerTiles();
                for (int p = 0; p < buildTiles.GetLength(1); p++)
                {
                    GameObject tile = buildTiles[player, p];
                    if (tile.GetComponent<Tile>().GetBuilding() == null && rmap.NoUnitPresent(tile.GetComponent<Tile>().GetX(), tile.GetComponent<Tile>().GetY()))
                    {
                        rmap.Build(GameObject.Find("GameControls").GetComponent<BuildButtons>().library, tile.GetComponent<Tile>().GetX(), tile.GetComponent<Tile>().GetY());
                        pCon.goldChange(GetPlayerID(), -20);
                        break;
                    }
                }
            }
        }
    }






    public int GetPlayerID()
    {
        return turnState;
    }

    //Handles transistion from one player to the next and from one phase to the next
	 public void EndTurn(){
		turnState += 1;
		sounds.PlayOneShot (endTurnClip);
		while((turnState < rmap.numPlayers) && !pCon.living(turnState)){
            turnState += 1;
		}
        //cam.setBase(rmap.buildings[turnState].GetComponent<buildingStats>().GetX(), rmap.buildings[turnState].GetComponent<buildingStats>().GetY());
        if(turnState < rmap.numPlayers)
        {
            cam.setBase(rmap.buildings[turnState].GetComponent<buildingStats>().GetX(), rmap.buildings[turnState].GetComponent<buildingStats>().GetY());
            cam.changeState(false);
        }
        //Debug.Log(turnState + " " + rmap.buildings[turnState].GetComponent<buildingStats>().GetX());
        if (turnState < rmap.numPlayers) {
			endText.text = "End Turn";

			//Apply next player restrictions

			//change to new player's name
			playerText.text = pCon.getName(turnState);
            
            //Set limits on clickable tiles

            //Change texts to currrent player values

            //Increment player gold
            pCon.incomeTick(turnState);
		} else {
            //Trigger phase 2
			endButton.interactable = false;
			int units = 0;
            //spawn units
            foreach(GameObject building in rmap.buildings)
            {
                if (building.GetComponent<buildingStats>().GetUnit() != null)
                {
                    rmap.ObjInstant(building.GetComponent<buildingStats>().GetUnit(), 3, 2, building.GetComponent<buildingStats>().GetX(), building.GetComponent<buildingStats>().GetY(), -1, -1); //currently all targets are random
					units++;
                }
            }
			//Debug.Log("Loop 1");

            cam.changeState(true);
			playerText.text = "Battle Phase";


            /*while (units != 0)
            {
                Debug.Log("in while");
                //endButton.
                /*foreach (GameObject unit in GameObject.Find("MapGen").GetComponent<RandomMap>().units)
                {
                    unit.GetComponent<UnitBehave>().StartTurn();
                    unit.GetComponent<UnitBehave>().Move();

                }

                //checking for each units death

                for (int p = rmap.units.Count - 2; p > -2; p--)
                {
                    GameObject unit = rmap.units[p + 1];
                    if (unit.GetComponent<UnitBehave>().GetHP() <= 0)
                    {
                        rmap.GetComponent<RandomMap>().units.Remove(unit);
                        DestroyImmediate(unit);
                        units--;
                    }
                }

                //checking for each buildings death

                for (int p = rmap.buildings.Count - 2; p > -2; p--)
                {
                    GameObject building = rmap.buildings[p + 1];
                    if (building.GetComponent<buildingStats>().GetHP() <= 0)
                    {
                        rmap.GetComponent<RandomMap>().buildings.Remove(building);
                        DestroyImmediate(building);
                    }
                }
                /*waiter = true;
				while(waiter){
					if (timer >= 0.5f) {
						waiter = false;
						timer = 0;
					}
				}
                //yield return new WaitForSeconds(1);

                //delete once we have attack, unless you like it like this, then just delete the while loop part of this

                //Debug.Log("Outside");
                //turnState = -1;
                playerCount = 0;
                foreach (GameObject building in rmap.buildings)
                {
                    if (building.transform.name == "BASE(Clone)") playerCount++;
                    else
                    {
                        break;
                    }
                }
                //Debug.Log("Once again");

                if (playerCount <= 1)
                {
                    //Debug.Log("Why??");
                    UnityEngine.SceneManagement.SceneManager.LoadScene(2);
                }
                //Debug.Log("");
                //endButton.interactable = true;
                //EndTurn();
            }*/
		}
        GameObject.Find("MapGen").GetComponent<RandomMap>().DestroyGhosts(); // destroy all the ghost buildings
    }

}
