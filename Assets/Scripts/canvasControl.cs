﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class canvasControl : MonoBehaviour {
    GameObject c1;
    GameObject c2;
	// Use this for initialization
	void Start () {
        c1 = transform.FindChild("settings").gameObject;
        c2 = transform.FindChild("Canvas").gameObject;
	}
	
	// Update is called once per frame
	void Update () {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            c1.SetActive(true);
            c2.SetActive(false);
        }
	}
}
