﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class buildingStats : MonoBehaviour {
    public int team;
    //public int gold;
    public int cost;
    public int attack; // Some buildings deal damage
    //public int defence;
    public int health; //health tokens. ensures buildings last
    public GameObject unit; // for unit producing structures

    public int x;
    public int y;

    Transform f1;
    Transform f2;
    Transform f3;
    //bool building = true;
    // Use this for initialization
    void Start () {
        f1 = transform.FindChild("flame1");
        f2 = transform.FindChild("flame2");
        f3 = transform.FindChild("flame3");

    }

    // Update is called once per frame
    void Update () {
		
	}

    //removes 1 health token
    public void damage() {
        health -= 1;
        
        if (f2.gameObject.active)
        {
            f3.gameObject.SetActive(true);
        }

        if (f1.gameObject.active)
        {
            f2.gameObject.SetActive(true);
        }

        f1.gameObject.SetActive(true);
    }

    //returns the owner of the building
    public int GetTeam()
    {
        return team;
    }
    
    //sets owner of the building
    public void SetTeam(int x)
    {
        team = x;
    }

    //returns health
    public int GetHP()
    {
        return health;

    }

    //returns unit
    public GameObject GetUnit()
    {
        return unit;
    }

    public void SetX(int i) {
        x = i;
    }

    public void SetY(int i)
    {
        y = i;
    }

    public int GetX()
    {
        return x;
    }

    public int GetY()
    {
        return y;
    }
}
