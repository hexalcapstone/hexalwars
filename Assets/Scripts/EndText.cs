﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EndText : MonoBehaviour {

	// Use this for initialization
	void Start () {
        int winner = GameObject.Find("Menu options").GetComponent<GameInfo>().winner + 1;
        if (winner >= 0)
            this.GetComponent<UnityEngine.UI.Text>().text = "Nicely done player " + winner;
        else this.GetComponent<UnityEngine.UI.Text>().text = "It is a draw";
        this.GetComponent<UnityEngine.UI.Text>().alignment = TextAnchor.MiddleCenter;
    }
	
	// Update is called once per frame
	void Update () {
        this.GetComponent<UnityEngine.UI.Text>().color = Random.ColorHSV(0f, 1f, 1f, 1f, 0.5f, 1f);
    }
}
