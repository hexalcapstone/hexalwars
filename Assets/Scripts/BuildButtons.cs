﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BuildButtons : MonoBehaviour
{
    public UnityEngine.UI.Text builderText;
    public GameObject barrack;
    public GameObject range;
    public GameObject library;
    public GameObject mine;
    public GameObject tower;
    public RandomMap rmap;
    public PlayerController pCon;
    public TurnController tCon;
    // Use this for initialization
    void Start()
    {
        rmap = GameObject.Find("MapGen").GetComponent<RandomMap>();
        pCon = GameObject.Find("PlayerController").GetComponent<PlayerController>();
        tCon = GameObject.Find("GameControls").GetComponent<TurnController>();
    }

    // Update is called once per frame
    void Update()
    {

    }

    void textChanged(string words)
    {
        builderText.text = words;
    }

    //interprets button presses.
    public void buttonPressed(int bCode)
    {
        if (bCode == 1 && pCon.GetGold(tCon.GetPlayerID())> 7)
        {
            //Barracks
            pCon.goldChange(tCon.GetPlayerID(), -8);
            textChanged("Barracks");
            rmap.GhostBuild(barrack);

        }
        else if (bCode == 3 && pCon.GetGold(tCon.GetPlayerID()) > 11)
        {
            //Ranged
            pCon.goldChange(tCon.GetPlayerID(), -12);
            textChanged("Ranged");
            rmap.GhostBuild(range);

        }
        else if (bCode == 2 && pCon.GetGold(tCon.GetPlayerID()) > 19)
        {
            //Special
            pCon.goldChange(tCon.GetPlayerID(), -20);
            textChanged("Special");
            rmap.GhostBuild(library);

        }
        else if (bCode == 4 && pCon.GetGold(tCon.GetPlayerID()) > 14)
        {
            //Income
            pCon.goldChange(tCon.GetPlayerID(), -15);
            textChanged("Income");
            rmap.GhostBuild(mine);

        }
        else if (bCode == 5 && pCon.GetGold(tCon.GetPlayerID()) > 9)
        {
            //Tower
            pCon.goldChange(tCon.GetPlayerID(), -10);
            textChanged("Tower");
            rmap.GhostBuild(tower);

        }
        else
        {
            textChanged("None");
            //clear selection
        }

    }
}