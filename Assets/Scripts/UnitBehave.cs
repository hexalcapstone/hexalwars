﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


/*
    Jason's guide to this shit coding:
    Methods speak from themselves
*/


public class UnitBehave : MonoBehaviour {

    public RandomMap manager; //The main script randomMap
    private int[] pos; //current position on the [,]
    public int team; // which team its on
    private int[,] map;
    
    public int[] target; //target position
    private int moveP; //Movement Points
    public int remainMP; //Remaining movment points 
    private int healthP; // Health Points

    public int type; // 0 melee, 1 archer, 2 s1, 3 s2, 4,s3
    private int fatigue = 0;

    public bool finished;

//	public AudioClip unitDeath;
//	public AudioSource sounds;

    // Use this for initialization - its a failure
    public void Start () {
        manager = GameObject.Find("MapGen").GetComponent<RandomMap>();
        RandomTarget();


    }
	
	// Update is called once per frame
	void Update () {
        float plz = 0.3f; //movement speed
        finished = true;
        Debug.Log(target[0] + " " + target[1] + " " + pos[0] + " " + pos[1]);
        if(manager.map[pos[0], pos[1]].transform.position.x - plz > transform.position.x )
        {
            //Debug.Log("x+");
            transform.position = transform.position + new Vector3(plz, 0, 0);
            finished = false;
        }
        else if (manager.map[pos[0], pos[1]].transform.position.x + plz < transform.position.x)
        {
            //Debug.Log("x-");
            transform.position = transform.position + new Vector3(-plz, 0, 0);
            finished = false;
        }
        else if(manager.map[pos[0], pos[1]].transform.position.x != transform.position.x)
        {
            transform.position = new Vector3(manager.map[pos[0], pos[1]].transform.position.x, transform.position.y, transform.position.z);
            finished = false;
        }


        if (manager.map[pos[0], pos[1]].transform.position.z - plz > transform.position.z)
        {
           // Debug.Log("z+");
            transform.position = transform.position + new Vector3(0, 0, plz);
            finished = false;
        }
        else if (manager.map[pos[0], pos[1]].transform.position.z + plz < transform.position.z)
        {
            //Debug.Log("z-");
            transform.position = transform.position + new Vector3(0, 0, -plz);
            finished = false;
        }
        else if (manager.map[pos[0], pos[1]].transform.position.z != transform.position.z)
        {
            transform.position = new Vector3(transform.position.x, transform.position.y, manager.map[pos[0], pos[1]].transform.position.z);
            finished = false;
        }

        for (int p = manager.units.Count - 2; p > -2; p--)
        {
            GameObject unit = manager.units[p + 1];
            if (unit != gameObject && unit.GetComponent<UnitBehave>().GetTeam() != team && Vector3.Distance(unit.transform.position, transform.position) < manager.objSize + manager.objSize / 2) //for safety
            {
                gameObject.GetComponent<UnitBehave>().Attack(unit);
                unit.GetComponent<UnitBehave>().Attack(gameObject);
                remainMP = -1;
            }
        }

        for (int p = manager.buildings.Count - 2; p > -2; p--)
        {
            GameObject building = manager.buildings[p + 1];
            if (building != gameObject && building.GetComponent<buildingStats>().GetTeam() != team && Vector3.Distance(building.transform.position, transform.position) < manager.objSize + manager.objSize / 2) //for safety
            {
                gameObject.GetComponent<UnitBehave>().Attack(building); //Slight Problem here, buidlings attacking player?
                remainMP = -1;
            }
        }
    }

    public int GetHP()
    {
        return healthP;
    }

    public void SetHP(int HP)
    {
        healthP = HP;
    }

    public int GetMP()
    {
        return moveP;
    }

    public void SetMP(int MP)
    {
        moveP = MP;
    }

    public int[] GetTarget()
    {
        return target;
    }

    public void SetTeam(int v)
    {
        team = v;
    }

    public int[] GetPos()
    {
        return pos;
    }

    public int GetTeam()
    {
        return team;
    }

    public void SetPos(int[] pos)
    {
        this.pos = pos;
    }

    //stuff for when you start the unit turn
    public void StartTurn()
    {
        remainMP = moveP;
    }

    //setting you unit to attack a random base
    public void RandomTarget()
    {
        //Debug.Log("Previous Target: " + target[0] + ", " + target[1]);
        int bases = 0;
        manager = GameObject.Find("MapGen").GetComponent<RandomMap>();
        for (int i = 0; i< manager.numPlayers; i++)
        {
            if(manager.buildings.Count > i)
            {
                if(manager.buildings[i].transform.name == "BASE(Clone)") //please change name if base name changes
                {
                    bases++;
                }
            }
        }
        int ran = UnityEngine.Random.Range(0, bases);
        //Debug.Log("ran: " + ran + " bases: " + bases);
        if(manager.buildings[ran].GetComponent<buildingStats>().GetTeam() == team)
        {
            //Debug.Log("same team");
            ran++;
            if(ran >= bases)
            {
                ran = 0;
            }
        }
        //Debug.Log("New target: " + manager.buildings[ran].GetComponent<buildingStats>().GetX() + ", " + manager.buildings[ran].GetComponent<buildingStats>().GetY());
        SetTarget(manager.buildings[ran].GetComponent<buildingStats>().GetX(), manager.buildings[ran].GetComponent<buildingStats>().GetY());
    }

    //creates a movement map for the unit to move toward
    public void SetTarget(int tarX, int tarY)
    {
        manager = GameObject.Find("MapGen").GetComponent<RandomMap>();
        map = new int[manager.radius * 2, manager.radius * 2];
        int[,] prev = new int[manager.radius * 2, manager.radius * 2];
        for (int p = 0; p < manager.radius * 2; p++)
        {
            for (int q = 0; q < manager.radius * 2; q++)
            {
                if (manager.map[p, q] == null || manager.map[p, q].GetComponent<Renderer>().material.color == Color.grey)
                {
                    map[p, q] = -1;
                    prev[p, q] = -1;
                }
                else
                {
                    map[p, q] = 0;
                    prev[p, q] = 0;
                }
            }
        }

        target = new int[2];
        target[0] = tarX;
        target[1] = tarY;
        //Debug.Log("Target: " + target[0] + ", " + target[1]);

        for (int p = 0; p < manager.radius * 2; p++)
        {
            for (int q = 0; q < manager.radius * 2; q++)
            {
                if (map[p,q] != -1)
                {
                    map[p, q] = 0;
                }
            }
        }

        map[tarX, tarY] = manager.radius * 2 + 4; // + 4 for safety

        //actual movement map creation
        bool same = false;
        while (!same)
        {
            prev = map;
            same = true;
            for (int p = 0; p < manager.radius * 2; p++)
            {
                for (int q = 0; q < manager.radius * 2; q++)
                {
                    if (map[p,q] > 0)
                    {
                        if(p + 1 < manager.radius * 2 && map[p, q] > map[p + 1, q] + 1 && map[p + 1, q] != -1)
                        {
                            map[p + 1, q] = map[p, q] - 1;
                            same = false;
                        }
                        if (q + 1 < manager.radius * 2 && p - 1 > -1 && map[p, q] > map[p, q + 1] + 1 && map[p, q + 1] != -1)
                        {
                            map[p, q + 1] = map[p, q] - 1;
                            same = false;
                        }
                        if (p + 1 < manager.radius * 2 && q + 1 < manager.radius * 2 && map[p, q] > map[p + 1, q + 1] + 1 && map[p + 1, q + 1] != -1)
                        {
                            map[p + 1, q + 1] = map[p, q] - 1;
                            same = false;
                        }
                        if (p - 1 > -1 && map[p, q] > map[p - 1, q] + 1 && map[p - 1, q] != -1)
                        {
                            map[p - 1, q] = map[p, q] - 1;
                            same = false;
                        }
                        if (q - 1 > -1 && map[p, q] > map[p, q - 1] + 1 && map[p, q - 1] != -1)
                        {
                            map[p, q - 1] = map[p, q] - 1;
                            same = false;
                        }
                        if (p - 1 > -1 && q - 1 > -1 && map[p, q] > map[p - 1, q - 1] + 1 && map[p - 1, q - 1] != -1)
                        {
                            map[p - 1, q - 1] = map[p, q] - 1;
                            same = false;
                        }
                    }
                }
            }
        }
    }


    //Moves the unit and has destroying of an object if it dies
    public void Move()
    {
        foreach (GameObject unit in manager.units)
        {
           if(unit.GetComponent<UnitBehave>().GetPos() != pos)
            {
                map[unit.GetComponent<UnitBehave>().GetPos()[0], unit.GetComponent<UnitBehave>().GetPos()[1]] -= (manager.radius * 3 + 6);
            }
        }

        while (remainMP-- > 0)
        {
            /*while (manager.map[pos[0], pos[1]].transform.position.z != transform.position.z && manager.map[pos[0], pos[1]].transform.position.x != transform.position.x)
            {
                float plz = 0.1f; //movement speed
                if (manager.map[pos[0], pos[1]].transform.position.x + plz > transform.position.x)
                {
                    Debug.Log("x+");
                    transform.position = transform.position + new Vector3(plz, 0, 0);
                }
                else if (manager.map[pos[0], pos[1]].transform.position.x - plz < transform.position.x)
                {
                    Debug.Log("x-");
                    transform.position = transform.position + new Vector3(-plz, 0, 0);
                }
                else if (manager.map[pos[0], pos[1]].transform.position.x != transform.position.x)
                {
                    transform.position = new Vector3(manager.map[pos[0], pos[1]].transform.position.x, transform.position.y, transform.position.z);
                }

                if (manager.map[pos[0], pos[1]].transform.position.z + plz > transform.position.z)
                {
                    Debug.Log("z+");
                    transform.position = transform.position + new Vector3(0, 0, plz);
                }
                else if (manager.map[pos[0], pos[1]].transform.position.z - plz < transform.position.z)
                {
                    Debug.Log("z-");
                    transform.position = transform.position + new Vector3(0, 0, -plz);
                }
                else if (manager.map[pos[0], pos[1]].transform.position.z != transform.position.z)
                {
                    transform.position = new Vector3(transform.position.x, transform.position.y, manager.map[pos[0], pos[1]].transform.position.z);
                }

                for (int p = manager.units.Count - 2; p > -2; p--)
                {
                    GameObject unit = manager.units[p + 1];
                    if (unit != gameObject && unit.GetComponent<UnitBehave>().GetTeam() != team && Vector3.Distance(unit.transform.position, transform.position) < manager.objSize + manager.objSize / 2) //for safety
                    {
                        gameObject.GetComponent<UnitBehave>().Attack(unit);
                        unit.GetComponent<UnitBehave>().Attack(gameObject);
                        remainMP = -1;
                    }
                }

                for (int p = manager.buildings.Count - 2; p > -2; p--)
                {
                    GameObject building = manager.buildings[p + 1];
                    if (building != gameObject && building.GetComponent<buildingStats>().GetTeam() != team && Vector3.Distance(building.transform.position, transform.position) < manager.objSize + manager.objSize / 2) //for safety
                    {
                        gameObject.GetComponent<UnitBehave>().Attack(building); //Slight Problem here, buidlings attacking player?
                        remainMP = -1;
                    }
                }
            }
            /*for (int p = manager.units.Count - 2; p > -2; p--)
            {
                GameObject unit = manager.units[p + 1];
                if (unit != gameObject && unit.GetComponent<UnitBehave>().GetTeam() != team && Vector3.Distance(unit.transform.position, transform.position) < manager.objSize + manager.objSize / 2) //for safety
                {
                    gameObject.GetComponent<UnitBehave>().Attack(unit);
                    unit.GetComponent<UnitBehave>().Attack(gameObject);
                    remainMP = -1;
                }
            }

            for (int p = manager.buildings.Count - 2; p > - 2; p--)
            {
                GameObject building = manager.buildings[p + 1];
                if (building != gameObject && building.GetComponent<buildingStats>().GetTeam() != team && Vector3.Distance(building.transform.position, transform.position) < manager.objSize + manager.objSize / 2) //for safety
                {
                    gameObject.GetComponent<UnitBehave>().Attack(building); //Slight Problem here, buidlings attacking player?
                    remainMP = -1;
                }
            }*/

            if (remainMP < 0) break;


            int possX = 0;
            int possY = 0;

            //top right
            if (map[pos[0] + possX, pos[1] + possY] < map[pos[0] + 1, pos[1] + 1])
            {
                possX = 1;
                possY = 1;
            }
            //bottom left
            if (pos[0] - 1 > -1 && pos[1] - 1 > -1 && map[pos[0] + possX, pos[1] + possY] < map[pos[0] - 1, pos[1] - 1])
            {
                possX =  - 1;
                possY =  - 1;
            }
            else if (pos[0] - 1 > -1 && pos[1] - 1 > -1 && map[pos[0] + possX, pos[1] + possY] == map[pos[0] - 1, pos[1] - 1] && UnityEngine.Random.Range(0f, 1f) < 0.5)
            {
                possX = -1;
                possY = -1;
            }
            //top left
            if (pos[0] - 1 > -1 && map[pos[0] + possX, pos[1] + possY] < map[pos[0] - 1, pos[1]])
            {
                possX =  - 1;
                possY = 0;
            }
            else if (pos[0] - 1 > -1 && map[pos[0] + possX, pos[1] + possY] == map[pos[0] - 1, pos[1]] && UnityEngine.Random.Range(0f, 1f) < 0.5)
            {
                possX = -1;
                possY = 0;
            }
            //bottom right
            if (map[pos[0] + possX, pos[1] + possY] < map[pos[0] + 1, pos[1]])
            {
                possX = 1;
                possY = 0;
            }
            else if (map[pos[0] + possX, pos[1] + possY] == map[pos[0] + 1, pos[1]] && UnityEngine.Random.Range(0f, 1f) < 0.5)
            {
                possX = 1;
                possY = 0;
            }
            //bottom
            if (pos[1] - 1 > -1 && map[pos[0] + possX, pos[1] + possY] < map[pos[0], pos[1] - 1])
            {
                possX = 0;
                possY = -1;
            }
            else if (pos[1] - 1 > -1 && map[pos[0] + possX, pos[1] + possY] < map[pos[0], pos[1] - 1] && UnityEngine.Random.Range(0f, 1f) < 0.5)
            {
                possX = 0;
                possY = -1;
            }
            //top
            if (map[pos[0] + possX, pos[1] + possY] < map[pos[0], pos[1] + 1])
            {
                possX = 0;
                possY = 1;
            }
            else if (map[pos[0] + possX, pos[1] + possY] < map[pos[0], pos[1] + 1] && UnityEngine.Random.Range(0f, 1f) < 0.5)
            {
                possX = 0;
                possY = 1;
            }
            //Debug.Log(pos[0] + ", " + pos[1] + ",    " + target[0] + ", " + target[1]);
            //Debug.Log(possX + ", " + possY);
            if(manager.map[pos[0] + possX, pos[1] + possY] != null && manager.map[pos[0] + possX, pos[1] + possY].GetComponent<Renderer>().material.color != Color.grey)
            {
                //transform.position = manager.map[pos[0] + possX, pos[1] + possY].transform.position + Vector3.up;
                pos[0] += possX;
                pos[1] += possY;
            }

            if (target[0] == pos[0] && target[1] == pos[1]) RandomTarget();
        }

        //resetting map movement
        for (int p = 0; p < manager.radius * 2; p++)
        {
            for (int q = 0; q < manager.radius * 2; q++)
            {
                if (map[p, q] < -1)
                {
                    map[p, q] += manager.radius * 3 + 6;
                }
            }
        }
    }


    //For the attacking issue of stuff
    private void Attack(GameObject unit)
    {
		//sounds = GameObject.Find ("Main Camera").GetComponent<AudioSource>();
		//sounds.PlayOneShot (unitDeath);
        if (unit.GetComponent<buildingStats>() != null) {
            int rollOne = UnityEngine.Random.Range(1, 21) + fatigue;
            int rollTwo = UnityEngine.Random.Range(1, 21) + unit.GetComponent<buildingStats>().attack;

            if (rollOne > rollTwo)
            {
                fatigue -= 1;
                unit.GetComponent<buildingStats>().damage();
            }
            else if (rollTwo > rollOne)
            {
                SetHP(0);
            }
            else if(rollTwo == rollOne)
            {
                Attack(unit);
            }
        }
        else if(unit.GetComponent<UnitBehave>() != null)
        {
            if (type == 0)//this is melee
            #region
            {
                if (unit.GetComponent<UnitBehave>().type == 1)  //melee vs archer
                {
                    int rollOne = UnityEngine.Random.Range(1, 21)+ fatigue;      //melee
                    int rollTwo = UnityEngine.Random.Range(1, 21) + unit.GetComponent<UnitBehave>().fatigue + 10; //archer

                    if (rollOne > rollTwo) {
                        fatigue -= 1;
                        unit.GetComponent<UnitBehave>().SetHP(0);
                        //rollTwo  loses unit loses
                    }
                    else if (rollTwo > rollOne)
                    {
                        unit.GetComponent<UnitBehave>().fatigue -= 1;
                        SetHP(0);
                        //rollOne loses this loses
                    }

                    else if(rollTwo == rollOne)
                    {
                        Attack(unit);
                    }
                }
                else if (unit.GetComponent<UnitBehave>().type == 2)  //melee vs mage
                {
                    int rollOne = UnityEngine.Random.Range(1, 21) + fatigue + 10;      //melee
                    int rollTwo = UnityEngine.Random.Range(1, 21) + unit.GetComponent<UnitBehave>().fatigue; //mage

                    if (rollOne > rollTwo)
                    {
                        fatigue -= 1;
                        unit.GetComponent<UnitBehave>().SetHP(0);
                        //rollTwo  loses unit loses
                    }
                    else if (rollTwo > rollOne)
                    {
                        unit.GetComponent<UnitBehave>().fatigue -= 1;
                        SetHP(0);
                        //rollOne loses this loses
                    }

                    else if (rollTwo == rollOne)
                    {
                        Attack(unit);
                    }
                }
                else
                {
                    int rollOne = UnityEngine.Random.Range(1, 21) + fatigue;
                    int rollTwo = UnityEngine.Random.Range(1, 21) + unit.GetComponent<UnitBehave>().fatigue;

                    if (rollOne > rollTwo)
                    {
                        unit.GetComponent<UnitBehave>().SetHP(0);
                        fatigue -= 1;
                        //rollTwo  loses unit loses
                    }
                    else if (rollTwo > rollOne)
                    {
                        unit.GetComponent<UnitBehave>().fatigue -= 1;
                        SetHP(0);
                        //rollOne loses this loses
                    }

                    else if (rollTwo == rollOne)
                    {
                        Attack(unit);
                    }
                }
            }
            #endregion
            else if(type == 1)// this is archer
            #region
            {
                if (unit.GetComponent<UnitBehave>().type == 2)  //melee vs archer
                {
                    int rollOne = UnityEngine.Random.Range(1, 21) + fatigue;      //archer
                    int rollTwo = UnityEngine.Random.Range(1, 21) + unit.GetComponent<UnitBehave>().fatigue + 10; //mage

                    if (rollOne > rollTwo)
                    {
                        fatigue -= 1;
                        unit.GetComponent<UnitBehave>().SetHP(0);
                        //rollTwo  loses unit loses
                    }
                    else if (rollTwo > rollOne)
                    {
                        unit.GetComponent<UnitBehave>().fatigue -= 1;
                        SetHP(0);
                        //rollOne loses this loses
                    }

                    else if (rollTwo == rollOne)
                    {
                        Attack(unit);
                    }
                }
                else if (unit.GetComponent<UnitBehave>().type == 0)  //melee vs mage
                {
                    int rollOne = UnityEngine.Random.Range(1, 21) + 10 + fatigue;         //archer
                    int rollTwo = UnityEngine.Random.Range(1, 21) + unit.GetComponent<UnitBehave>().fatigue;              //melee

                    if (rollOne > rollTwo)
                    {
                        fatigue -= 1;
                        unit.GetComponent<UnitBehave>().SetHP(0);
                        //rollTwo  loses unit loses
                    }
                    else if (rollTwo > rollOne)
                    {
                        unit.GetComponent<UnitBehave>().fatigue -= 1;
                        SetHP(0);
                        //rollOne loses this loses
                    }

                    else if (rollTwo == rollOne)
                    {
                        Attack(unit);
                    }
                }
                else
                {
                    int rollOne = UnityEngine.Random.Range(1, 21) + fatigue;
                    int rollTwo = UnityEngine.Random.Range(1, 21) + unit.GetComponent<UnitBehave>().fatigue;

                    if (rollOne > rollTwo)
                    {
                        fatigue -= 1;
                        unit.GetComponent<UnitBehave>().SetHP(0);
                        //rollTwo  loses unit loses
                    }
                    else if (rollTwo > rollOne)
                    {
                        unit.GetComponent<UnitBehave>().fatigue -= 1;
                        SetHP(0);
                        //rollOne loses this loses
                    }

                    else if (rollTwo == rollOne)
                    {
                        Attack(unit);
                    }
                }
            }
            #endregion
            else if(type == 2)// this is mage
            #region
            {
                if (unit.GetComponent<UnitBehave>().type == 0)  //mage vs melee
                {
                    int rollOne = UnityEngine.Random.Range(1, 21) + 10;      //mage
                    int rollTwo = UnityEngine.Random.Range(1, 21) + unit.GetComponent<UnitBehave>().fatigue + 10; //melee

                    if (rollOne > rollTwo)
                    {
                        fatigue -= 1;
                        unit.GetComponent<UnitBehave>().SetHP(0);
                        //rollTwo  loses unit loses
                    }
                    else if (rollTwo > rollOne)
                    {
                        unit.GetComponent<UnitBehave>().fatigue -= 1;
                        SetHP(0);
                        //rollOne loses this loses
                    }

                    else if (rollTwo == rollOne)
                    {
                        Attack(unit);
                    }
                }
                else if (unit.GetComponent<UnitBehave>().type == 1)  //mage vs archer
                {
                    int rollOne = UnityEngine.Random.Range(1, 21) + fatigue + 10;         //mage
                    int rollTwo = UnityEngine.Random.Range(1, 21) + unit.GetComponent<UnitBehave>().fatigue;              //archer

                    if (rollOne > rollTwo)
                    {
                        fatigue -= 1;
                        unit.GetComponent<UnitBehave>().SetHP(0);
                        //rollTwo  loses unit loses
                    }
                    else if (rollTwo > rollOne)
                    {
                        unit.GetComponent<UnitBehave>().fatigue -= 1;
                        SetHP(0);
                        //rollOne loses this loses
                    }

                    else if (rollTwo == rollOne)
                    {
                        Attack(unit);
                    }
                }
                else
                {
                    int rollOne = UnityEngine.Random.Range(1, 21) + fatigue;
                    int rollTwo = UnityEngine.Random.Range(1, 21) + unit.GetComponent<UnitBehave>().fatigue;

                    if (rollOne > rollTwo)
                    {
                        fatigue -= 1;
                        unit.GetComponent<UnitBehave>().SetHP(0);
                        //rollTwo  loses unit loses
                    }
                    else if (rollTwo > rollOne)
                    {
                        unit.GetComponent<UnitBehave>().fatigue -= 1;
                        SetHP(0);
                        //rollOne loses this loses
                    }

                    else if (rollTwo == rollOne)
                    {
                        Attack(unit);
                    }
                }
            }
            #endregion
        }
        
    }
}
