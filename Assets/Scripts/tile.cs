﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/*
    Jason's guide to this shit coding:
    Pretty simple
*/

public class Tile : MonoBehaviour {

    private int x;
    private int y;
    public int team;
    private GameObject occupied;
    private GameObject building;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void SetC(int x, int y)
    {
        this.x = x;
        this.y = y;
    }

    public int GetX()
    {
        return x;
    }

    public int GetY()
    {
        return y;
    }

    public void SetTeam(int team)
    {
        this.team = team;
    }

    public int GetTeam()
    {
        return team;
    }

    public void SetOccupied(GameObject occ)
    {
        this.occupied = occ;
    }

    public GameObject GetOccupied()
    {
        return occupied;
    }

    public void SetBuilding(GameObject build)
    {
        this.building = build;
    }

    public GameObject GetBuilding()
    {
        return building;
    }
}
