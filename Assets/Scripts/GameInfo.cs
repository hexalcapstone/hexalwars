﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameInfo : MonoBehaviour {
    public int Players;
    public int AI;
    public int MapRadius;
    public int height;
    public int width;
    public int winner;


	// Use this for initialization
	void Start () {
        winner = -1;
        Players = 2;
        AI = 0;
        MapRadius = 7;
        height = 3;
        width = 3;
        DontDestroyOnLoad(this);
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
