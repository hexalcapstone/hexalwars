﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour {


    //Inner Player class
	public class Player{
		string name;
		int gold;
		int income;
		int baseLocX;
		int baseLocY;
		int [][] baseTiles;
		int[] structures;
		bool alive;
		public GameObject MapGen;
		//list of owned buildings.

		//list of owned units.

        //Makes a player
		public Player(string newName) {//, int x, int y){
			// please save the base location here.

			gold = 10;
			income = 5;
			name = newName;
			alive = true;
			/*baseLocX = x;
			baseLocY = y;
			baseTiles[0][0] = x;
			baseTiles[0][1] = y;*/

			//int X;
			//int Y;



			/*gold = 50;
			income = 50;
			name = newName;
			alive = true;
			baseLocX = x;
			baseLocY = y;
			baseTiles[0][0] = x;
			baseTiles[0][1] = y;
			//structures[0] =1;   //base
			//structures[1] =0;   //mine
			structures[2] =0;   //melee building
			structures[3] =0;   //archer building
			structures[4] =0;   //mage building
			structures[5] =0;   //tower
            */

            structures = new int[] {1,0,0,0,0,0 };
		}


		public int[] getBase(){
			int[] ans = new int[2];
			ans [0] = baseLocX;
			ans [1] = baseLocY;
			return ans;
		}

        //Return player's name
		public string getName(){
			return name;
		}

		public void addTile(int X, int Y){
			baseTiles [baseTiles.Length] [0] = X;
			baseTiles [baseTiles.Length] [1] = Y;

		}

        //Add to owned structures
		public void addStructure(int key){
			structures [key] = structures [key] + 1;

		}
        
        public int GetGold()
        {
            return gold;
        }

        //Update gold
		public void goldChange(int value){
			gold = gold + value;
		}

		public int incomeTick(){
			goldChange (income);
			return income;
		}

		public void incer(){
			income += 5;
		}

        //alive?
		public bool isAlive(){
			return alive;
		}

        //kill
		public void die(){
			alive = false;
		}
	}



	int curPlayer;
	int roundStart;
	Player [] Players;
	public UnityEngine.UI.Text goldText;
    RandomMap rmap;

    void Start()
    {
        rmap = GameObject.Find("MapGen").GetComponent<RandomMap>();
        //initPC();
    }

    //Initialize players
    public void initPC(){
		Players = new Player[6];


        Players[0] = new Player("First");//, rmap.buildings[0].GetComponent<buildingStats>().GetX(), rmap.buildings[0].GetComponent<buildingStats>().GetY());
		//adding tiles



		Players [1] = new Player("Second");//, rmap.buildings[1].GetComponent<buildingStats>().GetX(), rmap.buildings[1].GetComponent<buildingStats>().GetY());
                                           //adding tiles




        Players [2] = new Player ("Third");//, rmap.buildings[2].GetComponent<buildingStats>().GetX(), rmap.buildings[2].GetComponent<buildingStats>().GetY());
                                           //adding tiles




        Players [3] = new Player ("Fourth");//, rmap.buildings[3].GetComponent<buildingStats>().GetX(), rmap.buildings[3].GetComponent<buildingStats>().GetY());
                                            //adding tiles




        Players [4] = new Player ("Fifth");//, rmap.buildings[4].GetComponent<buildingStats>().GetX(), rmap.buildings[4].GetComponent<buildingStats>().GetY());
                                           //adding tiles




        Players [5] = new Player ("Sixth");//, rmap.buildings[5].GetComponent<buildingStats>().GetX(), rmap.buildings[5].GetComponent<buildingStats>().GetY());
                                           //adding tiles




    }

    //Get Specific player name
	public string getName(int key){
		return Players [key].getName ();
	}

    //Update player's gold
	public void goldChange(int key, int value){
		Players [key].goldChange (value);
		goldText.text = value.ToString();
	}

    //Update income text
	public void incomeTick(int key){
		goldText.text = Players [key].incomeTick ().ToString ();

	}

    //Kill a player
	public void playerDeath(int key){
		Players [key].die ();

	}

    public int GetGold(int key)
    {
        if(key >= 0)
        return Players[key].GetGold();
        return 0;
    }

    //Check if a player is still alive
	public bool living(int key){
		return Players [key].isAlive ();
	}

    //Get a player
    public Player getPlayer(int i)
    {
        return Players[i];
    }

	public void incInc(int i){
		Players [i].incer ();
	}

    // Update is called once per frame
    void Update()
    {
        goldText.text = GetGold(GameObject.Find("GameControls").GetComponent<TurnController>().GetPlayerID()).ToString();
    }

}
