﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SliderNum : MonoBehaviour {
    int current;
    int numPlayers;
    int AI;
    GameObject menu;
    GameObject sliderP;
    GameObject sliderH;
    GameObject sliderW;
    GameObject sliderA;


    // Use this for initialization
    void Start () {
        current = 7;
        numPlayers = 2;
        menu = GameObject.Find("Menu options");
        sliderP = GameObject.Find("Slider P");
        sliderA = GameObject.Find("Slider A");
        sliderH = GameObject.Find("Slider H");
        sliderW = GameObject.Find("Slider W");
    }
	
	// Update is called once per frame
	void Update () {
        if(current != (int)this.GetComponent<UnityEngine.UI.Slider>().value)
        {
            current = (int)this.GetComponent<UnityEngine.UI.Slider>().value;
            this.GetComponentInChildren<UnityEngine.UI.Text>().text = current.ToString();
            menu.GetComponent<GameInfo>().MapRadius = current;

            //change height and width
            sliderH.GetComponent<UnityEngine.UI.Slider>().maxValue = (int)(current / 2) - 1;
            sliderW.GetComponent<UnityEngine.UI.Slider>().maxValue = (int)(current / 2) - 1;
        }
        //I do not want to make a seperate script for the others
        if(sliderH.GetComponent<UnityEngine.UI.Slider>().value != menu.GetComponent<GameInfo>().height)
        {
            menu.GetComponent<GameInfo>().height = (int)sliderH.GetComponent<UnityEngine.UI.Slider>().value;
            sliderH.GetComponentInChildren<UnityEngine.UI.Text>().text = ((int)(sliderH.GetComponent<UnityEngine.UI.Slider>().value)).ToString();
        }
        if (sliderW.GetComponent<UnityEngine.UI.Slider>().value != menu.GetComponent<GameInfo>().width)
        {
            menu.GetComponent<GameInfo>().width = (int)sliderW.GetComponent<UnityEngine.UI.Slider>().value;
            sliderW.GetComponentInChildren<UnityEngine.UI.Text>().text = ((int)(sliderW.GetComponent<UnityEngine.UI.Slider>().value)).ToString();
        }
        if (sliderP.GetComponent<UnityEngine.UI.Slider>().value != menu.GetComponent<GameInfo>().Players)
        {
            menu.GetComponent<GameInfo>().Players = (int)sliderP.GetComponent<UnityEngine.UI.Slider>().value;
            sliderP.GetComponentInChildren<UnityEngine.UI.Text>().text = ((int)(sliderP.GetComponent<UnityEngine.UI.Slider>().value)).ToString();
            sliderA.GetComponent<UnityEngine.UI.Slider>().maxValue = sliderP.GetComponent<UnityEngine.UI.Slider>().value;
        }
        if (sliderA.GetComponent<UnityEngine.UI.Slider>().value != menu.GetComponent<GameInfo>().AI)
        {
            menu.GetComponent<GameInfo>().AI = (int)sliderA.GetComponent<UnityEngine.UI.Slider>().value;
            sliderA.GetComponentInChildren<UnityEngine.UI.Text>().text = ((int)(sliderA.GetComponent<UnityEngine.UI.Slider>().value)).ToString();
        }
    }
}
