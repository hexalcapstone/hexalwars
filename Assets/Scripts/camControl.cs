﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class camControl : MonoBehaviour {
    public GameObject mapObject;
    public RandomMap mapS;
    public GameObject center;
    public GameObject target;
    public int speed = 1;
    //public GameObject target;
    //public bool inPlace = true;
    public bool combat = false;

    GameObject nextLoc;

    public Camera mainCam;

    private float timer = 0;
    private float targetAngle = 0;
    const float rotationAmount = 1.5f;
    public float rDistance = 1.0f;
    private int track = 0;

    // Use this for initialization
    // Gets required components 
    void Start()
    {

        mapS = mapObject.GetComponent<RandomMap>();
        //mainCam = Camera.main;
        //setBase(0, 4);

    }

    // Update is called once per frame
    // uses combat variable to determine what should be done during each update
    void Update()
    {
        if (center == null && mapS!=null)
        {
            center = mapS.map[mapS.radius - 1, mapS.radius - 1];
        }
        if (!combat)
        {
            if (Input.GetKeyDown(KeyCode.LeftArrow))
            {
                targetAngle = -60.0f;
            }
            else if (Input.GetKeyDown(KeyCode.RightArrow))
            {
                targetAngle = 60.0f;
            }

            if (targetAngle != 0)
            {
                Rotate();
            }
        }
        else if (combat) {
            timer += Time.deltaTime;

           
            target = center;
            transform.LookAt(center.transform);
            if (timer >= 3)
            {
                track += 1;
                if (track >= 3)
                {
                    mainCam.transform.position = new Vector3(53, 150, 31);
                    track = -1;
                }
                else if (track == 0) {
                    mainCam.transform.position = new Vector3(143, 75, 31);
                }
                else
                {
                    targetAngle = 120.0f;
                }
                timer = 0;
                //track += 1;
            }
            if (targetAngle != 0)
            {
                Rotate();
            }
        }
    }

    //Moves camera to a player's base, allowing for rotation around it.
    public void setBase(int x, int y) {
        nextLoc = mapS.map[x,y];
        target = nextLoc;

        float vecX = nextLoc.transform.position.x - center.transform.position.x;
        float vecZ = nextLoc.transform.position.z - center.transform.position.z;
        Vector2 vecA = new Vector2(vecX,vecZ);
        
        mainCam.transform.position = new Vector3(vecA.x, 20, vecA.y);
        mainCam.transform.position = Vector3.Lerp(nextLoc.transform.position, mainCam.transform.position, 0.5f);
        mainCam.transform.Translate(0.0f, 11.0f, 0.0f);
        transform.LookAt(nextLoc.transform);

    }

    //Handles smooth rotation around target
    protected void Rotate()
    {


        if (targetAngle > 0)
        {
            transform.RotateAround(target.transform.position, Vector3.up, -rotationAmount);
            targetAngle -= rotationAmount;
        }
        else if (targetAngle < 0)
        {
            transform.RotateAround(target.transform.position, Vector3.up, rotationAmount);
            targetAngle += rotationAmount;
        }

    }

    //Switches the state of the camera to combat or !combat
    public void changeState(bool x)
    {
        combat = x;

        if (combat) {
            reset();

        }
    }

    //default position just in case
    public void reset()
    {
               
        mainCam.transform.position = new Vector3(143,75,31);
    }
}
