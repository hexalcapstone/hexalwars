﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ButtonControl : MonoBehaviour {

    public void NextLevelButton(int index)
    {
        if(index == 0)
        {
            Destroy(GameObject.Find("Menu options"));
        }
        UnityEngine.SceneManagement.SceneManager.LoadScene(index);
    }
}
